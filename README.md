# React Router project

## Hooks

1. Most modern way to build react apps
2. using functions for nearly everything
3. avoid 'this' confusion...
4. use react concepts - state and props
5. better models the way react works
6. easier to work with

**10** hooks but **3** commons -

1. useState - local state
2. useEffect - side effects
3. useContext - Access data in context

```javascript
import React, { useState } from "react";

function Example() {
  const [email, setEmail] = useState("");
  /**
   * array destructuring - useState returns an array
   *
   */

  return (
    <input
      type="text"
      value={email}
      onChange={event => setEmail(event.target.value)}
    />
  );
}
```

A fun challenge that is about to be improved.
