import React from 'react';

// to use a class, we extend like so
class AboutPage extends React.Component {
    render() {
        return (
            <>
                <h2>About</h2>
                <p>This app uses React.</p>
            </>
        )
    }
}

export default AboutPage;