import React, { useState, useEffect } from "react";
// import statement and tab... clever!
import { getCourses } from "../api/courseApi";
import CoursesList from "./CourseList";

function CoursesPage() {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    getCourses().then(_courses => setCourses(_courses));
  }, []); // dependency array

  return (
    <>
      <h2>Courses</h2>
      <CoursesList courses={courses} />
    </>
  );
}

export default CoursesPage;
